import 'package:bloc_tutorial/counter_cubit.dart';
import 'package:bloc_tutorial/page_a.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int _count = 0 ;

  final cubitA = CounterCubit(0) ;

  @override
  Widget build(BuildContext context) {

    cubitA.listen((value) {
      setState(() {
        _count = value ;
      });
    });

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 25),
            child: InkWell(
              child: Icon(Icons.refresh),
              onTap: (() {
                cubitA.increment();
              }),
            ),
          ),
        ],
      ),
      body: PageView(
        children: [
          PageA(cubitA, "A"),
          PageA(cubitA, "B")
        ],
      ),
    );
  }
}