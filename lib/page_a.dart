import 'package:bloc_tutorial/counter_cubit.dart';
import 'package:flutter/material.dart';

class PageA extends StatefulWidget {

  PageA(this.cubit, this.pageTag) : super();

  final CounterCubit cubit ;
  final String pageTag ;

  @override
  State<StatefulWidget> createState() {
    return PageAState();
  }
}

class PageAState extends State<PageA> {


  @override
  Widget build(BuildContext context) {

    int _count = widget.cubit.state ;

    widget.cubit.listen((value) {
        _count = value ;
    });

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "${widget.pageTag} : $_count",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 30
              ),
            ),
          ],
        ),
      ),
    );
  }

}